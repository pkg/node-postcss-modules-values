# Installation
> `npm install --save @types/postcss-modules-values`

# Summary
This package contains type definitions for postcss-modules-values (https://github.com/css-modules/postcss-modules-values#readme).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/postcss-modules-values.
## [index.d.ts](https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/postcss-modules-values/index.d.ts)
````ts
// Type definitions for postcss-modules-values 4.0
// Project: https://github.com/css-modules/postcss-modules-values#readme
// Definitions by: Bob Matcuk <https://github.com/bmatcuk>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped
// TypeScript Version: 2.2

import { PluginCreator } from "postcss";

declare namespace values {
    interface Options {
        createImportedName(name: string): string;
    }
}

declare const creator: PluginCreator<values.Options>;

export = creator;

````

### Additional Details
 * Last updated: Thu, 18 Nov 2021 19:01:41 GMT
 * Dependencies: [@types/postcss](https://npmjs.com/package/@types/postcss)
 * Global values: none

# Credits
These definitions were written by [Bob Matcuk](https://github.com/bmatcuk).
